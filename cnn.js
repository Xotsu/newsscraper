const https = require("https");
const fs = require('fs');
const jsdom = require("jsdom");
const mongoose = require('mongoose');
const visitedDB = require("./models/visited")
const economyDB = require("./models/economy")
const entertainmentDB = require("./models/entertainment")
const healthDB = require("./models/health")
const politicsDB = require("./models/politics")
const sportsDB = require("./models/sports")
const technologyDB = require("./models/technology")
const ukDB = require("./models/uk")
const worldDB = require("./models/world")
const { JSDOM } = jsdom;

const databaseTopics = [economyDB, technologyDB, worldDB, ukDB, entertainmentDB, sportsDB, politicsDB, healthDB]

let visited = []
let visitedTemp = []
let removingCount = 0


let cnnNews = {
	economy: [],
	technology: [],
	world: [],
	uk: [],
	entertainment: [],
	sports: [],
	politics: [],
	health: []
}

let cnnNewsLinks = [["economy", "https://edition.cnn.com/data/ocs/section/business/investing/index.html:investing-zone-", [], []], // Topic, topc link template, topic links, articles
["technology", "https://edition.cnn.com/data/ocs/section/business/tech/index.html:tech-zone-", [], []],
["world", "https://edition.cnn.com/data/ocs/section/world/index.html:world-zone-", [], []],
["uk", "https://edition.cnn.com/data/ocs/section/uk/index.html:uk-zone-", [], []],
["entertainment", "https://edition.cnn.com/data/ocs/section/entertainment/index.html:entertainment-zone-", [], []],
["sports", "https://edition.cnn.com/data/ocs/section/sport/index.html:sport-zone-", [], []],
["politics", "https://edition.cnn.com/data/ocs/section/politics/index.html:politics-zone-", [], []],
["health", "https://edition.cnn.com/data/ocs/section/health/index.html:health-zone-", [], []]]

let parseCNNTime = x => {
	let temp = []
	x = x.trim()
	x = x.split(" ")
	let timeSplit = x[1].split("")
	timeSplit.splice("2", 0, ":")
	timeSplit.splice("5", 0, ":00")
	x[1] = timeSplit.join("")

	temp.push(x[7], x[5], x[6], x[1])
	return new Date(temp.join(" ")).toString().substring(0, 24)
}

let cnnArticles = () => {
	let counter = 0
	let total = 0


	// Get visited list from the database and set it to visited

	async function getVisited() {
		// No need to `await` on this, mongoose 4 handles connection buffering
		// internally
		//mongodb://heroku_vtt0tqz6:t000ulkjh3arj7imaclvcvsi6@ds121182.mlab.com:21182/heroku_vtt0tqz6
		//mongodb://localhost:27017/newsContentAggregator
		//mongodb://heroku_vtt0tqz6:Justinas20020214@ds121182.mlab.com:21182/heroku_vtt0tqz6
		mongoose.connect("mongodb://Xotsu:Password123@ds121182.mlab.com:21182/heroku_vtt0tqz6", { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log("Connected to MongoDB"))

		//await visitedDB.create({ visited: visitedTemp });
		let dbVisited = await visitedDB.find()
		// Prints an array with 1 element, the above document
		try {
			for (let i = 0; i < dbVisited[0].visited.length; i++) {
				visited.push(dbVisited[0].visited[i])
			}
		} catch{ }

	}


	getVisited().then(() => {

		for (let x = 0; x < cnnNewsLinks.length; x++) {
			for (let y = 0; y < cnnNewsLinks[x][3].length; y++) {
				if (!visited.includes(cnnNewsLinks[x][3][y]) && !visitedTemp.includes(cnnNewsLinks[x][3][y])) {
					total++
					visitedTemp.push(cnnNewsLinks[x][3][y])
				}
			}
		}

		// async function sendVisited() {
		// 	// No need to `await` on this, mongoose 4 handles connection buffering
		// 	// internally
		// 	mongoose.connect("heroku_vtt0tqz6:Justinas20020214@ds121182.mlab.com:21182/heroku_vtt0tqz6/newsContentAggregator", { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log("Connected to MongoDB"))

		// 	await visitedDB.create({ visited: visitedTemp });

		// 	// Prints an array with 1 element, the above document
		// 	console.log(await visitedDB.find());
		// }
		//sendVisited().catch(error => console.error(error.stack));
		outerloop:
		for (let x = 0; x < cnnNewsLinks.length; x++) {
			for (let y = 0; y < cnnNewsLinks[x][3].length; y++) {
				if (!visited.includes(cnnNewsLinks[x][3][y])) {
					setTimeout(() => {
						https.get(cnnNewsLinks[x][3][y], function (res) {
							let body
							let author
							let headline
							let publicationDate
							res.on("data", (d) => {
								body += d
							});
							res.on("end", () => {
								console.log(counter, total)
								let html = new JSDOM(body)
								let imageUrl = "Article has no image"
								let url = cnnNewsLinks[x][3][y]
								let errorFree = true
								//author, tags, strapline, headline, image, url, publication date
								let img = html.window.document.getElementsByClassName("media__image")
								for (let i = 0; i < img.length; i++) {
									try {
										if (img[i].src.includes(".jpg")) { //|| img[i].src.includes(".png")
											imageUrl = img[i].src
											break
										}
									} catch{
									}
								}
								try {
									author = html.window.document.getElementsByClassName("metadata__byline__author")[0].textContent
								} catch{
									author = "author error url: " + url
									errorFree = false
								}
								try {
									headline = html.window.document.getElementsByClassName("pg-headline")[0].textContent
								} catch{
									headline = "headline error url: " + url
									errorFree = false
								}
								try {
									publicationDate = html.window.document.getElementsByClassName("update-time")[0].textContent
								} catch{
									publicationDate = "date error url: " + url
									errorFree = false
								}
								if (errorFree) {
									cnnNews[cnnNewsLinks[x][0]].push([headline, imageUrl, parseCNNTime(publicationDate), author, url, new Date()])
								}
								// console.log("URL: " + url)  //document.querySelector("#large-media > div > img")
								// console.log("Author: " + author)
								// console.log("Headline: " + headline)
								// console.log("IMG: " + imageUrl)
								// console.log("Date: " + publicationDate)
								body = ""
								if (total > 0) {
									counter++
								}
								if (counter == (total)) {
									fs.writeFile('cnnArticlesNew.txt', JSON.stringify(cnnNews, null, 2), function (err) {
										if (err) throw err;
										console.log('Saved file!');
									});
									// Update the visited list to the new visited list
									async function updateVisited() {
										// No need to `await` on this, mongoose 4 handles connection buffering
										// internally
										//await visitedDB.create({ visited: visitedTemp });
										await visitedDB.updateOne({ _id: "5ecc267a267f6049f82f5048" }, { $set: { visited: visited } })
										//let dbVisited = await visitedDB.find()
										// Prints an array with 1 element, the above document
										// console.log(typeof (cnnNews[cnnNewsLinks[0][0]][0]))
										console.log("Scraped " + visitedTemp.length + " new articles!");
									}//Inside of this .then i need to update all of the new articles to their respective area in the database
									//I also need to fetch all of the categories from the database and check if their date has expired past 5 days (test 1 min) then delete them.
									//Then call the .then(() => mongoose.connection.close())

									async function pushArticles() {
										for (let z = 0; z < cnnNewsLinks.length; z++) {
											for (let a = 0; a < cnnNews[cnnNewsLinks[z][0]].length; a++) {
												await databaseTopics[z].create({
													headline: cnnNews[cnnNewsLinks[z][0]][a][0],
													imageURL: cnnNews[cnnNewsLinks[z][0]][a][1],
													publicationDate: cnnNews[cnnNewsLinks[z][0]][a][2],
													author: cnnNews[cnnNewsLinks[z][0]][a][3],
													url: cnnNews[cnnNewsLinks[z][0]][a][4],
													timeStamp: cnnNews[cnnNewsLinks[z][0]][a][5]
												});
												// 	// Prints an array with 1 element, the above document
												// 	console.log(await visitedDB.find());
											}
										}
									}

									async function removeArticle(b, c, values, totalRemoving) {
										await databaseTopics[b].deleteOne({ _id: values[b][c]._id })
										removingCount++
										if (removingCount == totalRemoving) {
											console.log("Removed " + totalRemoving + " old articles")
											mongoose.connection.close()
										}
									}

									async function thePromise() {
										return await Promise.all([economyDB.find(), technologyDB.find(), worldDB.find(), ukDB.find(), entertainmentDB.find(), sportsDB.find(), politicsDB.find(), healthDB.find()])
									}

									updateVisited()
										.then(pushArticles)
										.then(thePromise)
										.then((values) => {
											let totalRemoving = 0
											for (let b = 0; b < values.length; b++) {
												for (let c = 0; c < values[b].length; c++) {
													if (Math.floor(((new Date() - values[b][c].timeStamp) / (24000 * 3600))) > 4) {
														totalRemoving++
													}
												}
											}
											if (totalRemoving == 0) {
												console.log("Removed " + totalRemoving + " old articles")
												mongoose.connection.close()
											} else {
												for (let b = 0; b < values.length; b++) {
													for (let c = 0; c < values[b].length; c++) {
														// if (Math.floor(((new Date() - values[b][c].timeStamp) / (1000 * 60))) > 2) //3 min {
														//await databaseTopics[b].deleteOne({ _id: values[b][c]._id })
														// }
														if (Math.floor(((new Date() - values[b][c].timeStamp) / (24000 * 3600))) > 0) {
															removeArticle(b, c, values, totalRemoving)
														}
													}
												}
											}
										})
										//.then(() => mongoose.connection.close())
										.catch(error => console.error(error));
									//updateVisited().then(() => mongoose.connection.close()).catch(error => console.error(error.stack));
								}
							})
						})
					}, 1000)
					visited.push(cnnNewsLinks[x][3][y])
				} else if (total == 0) {
					fs.writeFile('cnnArticlesNew.txt', JSON.stringify(cnnNews, null, 2), function (err) {
						if (err) throw err;
						console.log('Saved file!');
					});
					// Update the visited list to the new visited list
					async function updateVisited() {
						// No need to `await` on this, mongoose 4 handles connection buffering
						// internally
						//await visitedDB.create({ visited: visitedTemp });
						await visitedDB.updateOne({ _id: "5ebdab337cfcf705979ce158" }, { $set: { visited: visited } })
						//let dbVisited = await visitedDB.find()
						// Prints an array with 1 element, the above document
						console.log("Scraped " + visitedTemp.length + " new articles!");
					}
					updateVisited().then(() => {
						mongoose.connection.close()
						console.log("Closed server as total was 0")
					}).catch(error => console.error(error));
					break outerloop
				}
			}
		}
	}).catch(error => console.error(error));

}


let cnn = () => {
	let counter = 0
	for (let x = 0; x < cnnNewsLinks.length; x++) {
		for (let y = 1; y < 10; y++) {
			cnnNewsLinks[x][2].push(cnnNewsLinks[x][1] + `${y}/views/zones/common/zone-manager.izl`)
		}
		for (let z = 0; z < cnnNewsLinks[x][2].length; z++) {
			setTimeout(() => {
				https.get(cnnNewsLinks[x][2][z], function (res) {
					let body
					res.on("data", (d) => {
						body += d
					});
					res.on("end", () => {
						body = body.replace(/\\/g, "")
						body = `<!DOCTYPE html>${body.substring(41, body.length - 2)}`
						let html = new JSDOM(body)
						let article = html.window.document.querySelectorAll("a")
						for (let c = 0; c < article.length; c++) {
							try {
								if (!article[c].href.includes("/live-news/") && !article[c].href.includes("/article/") && article[c].href.includes("index.html") && !(cnnNewsLinks[x][3].includes("https://edition.cnn.com" + article[c].href)))
									if (article[c].href.includes("cnn.com")) {
										if (!article[c].href.includes("https")) {
											article[c].href = article[c].href.replace("http", "https")
										}
										article[c].href = article[c].href.replace("www.cnn", "edition.cnn")
										article[c].href = article[c].href.replace("//cnn.com", "//edition.cnn.com")
										cnnNewsLinks[x][3].push(article[c].href)
									} else if (article[c].href.includes("/gallery/")) {
									}
									else {
										cnnNewsLinks[x][3].push("https://edition.cnn.com" + article[c].href)
									}
								oldArticleCount = articleCount
								articleCount++

							} catch{
							}
						}
						body = ""
						counter++
						if (counter == (cnnNewsLinks.length * cnnNewsLinks[x][2].length)) {
							cnnArticles()
						}
					})
				})
			}, 1000)
		}
	}
}


cnn()




// let wsj = () => {
//     console.log("hi")
// }
// let guardian = () => {
//     console.log("hi")
// }
// let nyt = () => {
//     console.log("hi")
// }
// let bloomberg = () => {
//     console.log("hi")
// }
// let fox = () => {
//     console.log("hi")
// }
// let bbc = () => {
//     console.log("hi")
// }
// let abc = () => {
//     console.log("hi")
// }
// let scmp = () => {
//     console.log("hi")
// }



// // Update the visited array every time the loop goes through
// router.patch("/:postID", async (req, res) => {
// 	try {
// 		const updatedPost = await economyDB.updateOne({ _id: req.params.postID }, { $set: { economy: req.body.economy } })
// 	} catch (err) {
// 		res.json({ message: err })
// 	}
// })

// // Initially send the first visited array to the database
// router.post("/", async (req, res) => {
// 	const post = new visitedDB({
// 		visited: req.body.economy,
// 	})
// 	try {
// 		const savedPost = await post.save()
// 		res.json(savedPost)
// 	} catch (err) {
// 		res.json({ message: err })
// 	}
// })





// Initially send the first visited array to the database
// async function sendVisited(req, res) {
// 	const post = new visitedDB({
// 		visited: visitedTemp,
// 	})
// 	try {
// 		const savedPost = await post.save()
// 		res.json(savedPost)
// 	} catch (err) {
// 		res.json({ message: err })
// 	}
// }

// async function sendVisited() {
// 	// No need to `await` on this, mongoose 4 handles connection buffering
// 	// internally
// 	mongoose.connect("mongodb://localhost:27017/newsContentAggregator", { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log("Connected to MongoDB"))

// 	await visitedDB.create({ visited: visitedTemp });

// 	// Prints an array with 1 element, the above document
// 	console.log(await visitedDB.find());
// }


// ID of visited in database 5ebdab337cfcf705979ce158

// async function updateArticles() {
// 	// No need to `await` on this, mongoose 4 handles connection buffering
// 	// internally
// 	//await visitedDB.create({ visited: visitedTemp });
// 	let dbVisited = await visitedDB.find()
// 	// Prints an array with 1 element, the above document
// 	for (let i = 0; i < dbVisited[0].visited.length; i++) {
// 		visited.push(dbVisited[0].visited[i])
// 	}
// }


// updateArticles().then(() => mongoose.connection.close()).catch(error => console.error(error.stack));