import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Economy from "../views/Economy.vue";
import Technology from "../views/Technology.vue";
import World from "../views/World.vue";
import Uk from "../views/Uk.vue";
import Entertainment from "../views/Entertainment.vue";
import Sports from "../views/Sports.vue";
import Politics from "../views/Politics.vue";
import Health from "../views/Health.vue";
import Search from "../views/Search.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  }, {
    path: "/economy",
    name: "Economy",
    component: Economy
  }, {
    path: "/technology",
    name: "Technology",
    component: Technology
  }, {
    path: "/world",
    name: "World",
    component: World
  }, {
    path: "/uk",
    name: "Uk",
    component: Uk
  }, {
    path: "/entertainment",
    name: "Entertainment",
    component: Entertainment
  }, {
    path: "/sports",
    name: "Sports",
    component: Sports
  }, {
    path: "/politics",
    name: "Politics",
    component: Politics
  }, {
    path: "/health",
    name: "Health",
    component: Health
  },
  {
    path: "/search/:id",
    name: "Search",
    component: Search
  },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: function() {
  //     return import(/* webpackChunkName: "about" */ "../views/About.vue");
  //   }
  // }
];

const router = new VueRouter({
  routes
});

export default router;
