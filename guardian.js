//  Get articles for each of different topics from the guardian.
//  Compare the articles to the visited array.
//  If The article is not in viisted array add it to toVisit array.
//  Go through the to visit array parsing the res json and add to db.
//  Go through db and check if any articles are over x time long then remove if they are.

//  6895d950-7665-4368-9d52-499c1b8dfa56
//  ?api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/search?api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/uk-news?page-size=50&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/search?page-size=50&section=uk-news&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=test


//  https://content.guardianapis.com/technology?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/business/economics?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/world?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/uk-news?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/uk/lifeandstyle?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/uk/sport?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/politics?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56
//  https://content.guardianapis.com/global-development?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56

//const https = require("https");
const jsdom = require("jsdom");
const mongoose = require('mongoose');
const visitedDB = require("./models/visited")
const economyDB = require("./models/economy")
const entertainmentDB = require("./models/entertainment")
const healthDB = require("./models/health")
const politicsDB = require("./models/politics")
const sportsDB = require("./models/sports")
const technologyDB = require("./models/technology")
const ukDB = require("./models/uk")
const worldDB = require("./models/world")
const { JSDOM } = jsdom;
const axios = require('axios');

const databaseTopics = [economyDB, technologyDB, worldDB, ukDB, entertainmentDB, sportsDB, politicsDB, healthDB]
const articleSections = ["https://content.guardianapis.com/business/economics?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/technology?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/world?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/uk-news?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/uk/lifeandstyle?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/uk/sport?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/politics?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56",
    "https://content.guardianapis.com/global-development?page-size=50&order-by=newest&show-fields=headline%2Cbyline%2CfirstPublicationDate%2Cpublication%2Cthumbnail&show-tags=publication&api-key=6895d950-7665-4368-9d52-499c1b8dfa56"
]
let articles = [[], [], [], [], [], [], [], []]
let visited = []

let getCounter = 0
let visitedCounter = 0
let removingCount = 0




async function getVisited() {

    mongoose.connect("mongodb://Xotsu:Password123@ds121182.mlab.com:21182/heroku_vtt0tqz6", { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log("Connected to MongoDB"))

    let dbVisited = await visitedDB.find()
    try {
        for (let i = 0; i < dbVisited[0].visited.length; i++) {
            visited.push(dbVisited[0].visited[i])
        }
    } catch{
        console.log("DB visited error")
    }
}

async function updateVisited() {
    await visitedDB.updateOne({ _id: "5ecc267a267f6049f82f5048" }, { $set: { visited: visited } })
    console.log("Scraped " + visitedCounter + " new articles!");
}

async function pushArticles() {
    for (let z = 0; z < articles.length; z++) {
        for (let a = 0; a < articles[z].length; a++) {
            await databaseTopics[z].create({
                headline: articles[z][a][0],
                imageURL: articles[z][a][1],
                publicationDate: articles[z][a][2],
                author: articles[z][a][3],
                url: articles[z][a][4],
                timeStamp: articles[z][a][5]
            });
        }
    }
}

async function thePromise() {
    return await Promise.all([economyDB.find(), technologyDB.find(), worldDB.find(), ukDB.find(), entertainmentDB.find(), sportsDB.find(), politicsDB.find(), healthDB.find()])
}

async function removeArticle(b, c, values, totalRemoving) {
    await databaseTopics[b].deleteOne({ _id: values[b][c]._id })
    removingCount++
    if (removingCount == totalRemoving) {
        console.log("Removed " + totalRemoving + " old articles")
        mongoose.connection.close()
    }
}

let parseGuardianTime = x => {
    return new Date(x).toString().substring(0, 24)
}

let parseGuardianName = x => {
    return (x == "theguardian.com") ? "The Guardian" : x
}

function updateDB() {
    updateVisited()
        .then(pushArticles)
        .then(thePromise)
        .then((values) => {
            let totalRemoving = 0
            for (let b = 0; b < values.length; b++) {
                for (let c = 0; c < values[b].length; c++) {
                    if (Math.floor(((new Date() - values[b][c].timeStamp) / (24000 * 3600))) > 4) {
                        totalRemoving++
                    }
                }
            }
            if (totalRemoving == 0) {
                console.log("Removed " + totalRemoving + " old articles")
                mongoose.connection.close()
            } else {
                for (let b = 0; b < values.length; b++) {
                    for (let c = 0; c < values[b].length; c++) {
                        if (Math.floor(((new Date() - values[b][c].timeStamp) / (24000 * 3600))) > 0) {
                            removeArticle(b, c, values, totalRemoving)
                        }
                    }
                }
            }
        })
        .catch(error => console.error(error));
}


async function getUser(url, topicCounter) {
    try {
        let res = await axios.get(url);
        for (let x = 0; x < res.data.response.results.length; x++) {
            if (!visited.includes(res.data.response.results[x].webUrl)) {
                articles[topicCounter].push([res.data.response.results[x].webTitle,
                res.data.response.results[x].fields.thumbnail,
                parseGuardianTime(res.data.response.results[x].fields.firstPublicationDate),
                res.data.response.results[x].fields.byline + ", " + parseGuardianName(res.data.response.results[x].fields.publication),
                res.data.response.results[x].webUrl,
                new Date()
                ])
                visited.push(res.data.response.results[x].webUrl)
                visitedCounter++
            }

            //headline, imageUrl, parseCNNTime(publicationDate), author, url, new Date()
        }
        getCounter++
        if (getCounter == 8) {
            if (visitedCounter > 0) {
                updateDB()

            } else {
                mongoose.connection.close()
                console.log("There are no new articles from The Guardian")
            }
            //console.log(articles)
        }
    } catch (error) {
        console.error(error);
    }
}



getVisited().then(() => {
    for (let i = 0; i < articleSections.length; i++) {
        getUser(articleSections[i], i)
    }
}).catch(error => console.error(error));